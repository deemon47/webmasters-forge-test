//=require jquery/dist/jquery.min.js
//=require bootstrap-sass/assets/javascripts/bootstrap.min.js
"use strict";
// Защита от CSRF
$.ajaxSetup({
    headers : {
        'csrf_token': $('meta[name="csrf_token"]').attr('content')
    }
});
$(function () {
	// Активация подсказок
	$('[data-toggle="tooltip"]').tooltip();

	// Превью аватара
	var avatar_preview=$('#avatar_preview').hide();
	$("#avatar").change(function()
	{
	  if (this.files && this.files[0])
	  {
	    var reader = new FileReader();
	    reader.onload = function(e)
	    {
	      avatar_preview.find('img').attr('src', e.target.result);
	    }
	    reader.readAsDataURL(this.files[0]);
	    avatar_preview.show();
	  }
	  else
	  	avatar_preview.hide();
	});

})

//=require validation.js
//=require app.js