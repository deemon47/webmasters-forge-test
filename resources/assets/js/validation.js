/**
 * Базовое правило проверки
 * @param {string} f_name    имя поля
 * @param {array} params    параметры правила
 * @param {object} data      данные формы
 * @param {string} rule_name имя правила (для генерации ошибки)
 */
function BaseRule(f_name,params,data,rule_name)
{
	this.rule_name=rule_name;
	this.f_name=f_name;
	this.params=params;
	this.data=data;
	this.error_type=rule_name;
	this.value=data[f_name].value;
	this.check_on_empty=false;
	/**
	 * Основная логика проверки
	 * @return {Boolean} ture если поле прошло проверку
	 */
	this.isValid=function ()
	{
		app.log("method not overloaded",'','error');
	}
	/**
	 * Дополнительная проверка для исключения проверки на заполненность данных в каждом правиле
	 * @return {boolean} нужна проверка
	 */
	this.needCheck=function ()
	{
		return this.check_on_empty || this.value ;
	}
	/**
	 * Генерация локализации текста ошибки
	 * @return {stiring} локализованный текст ошибки
	 */
	this.getError=function ()
	{
		var data=$.extend({'field':app.trans('fields.'+this.f_name)},this.params);
		return app.trans('validation.'+this.error_type,data);
	}
}
/**
 * Объект перечня правил проверки
 * @type {object}
 */
var validation_rules=
{
	// Правило проверки на обязательность заполнения
	'required':function ()
	{
		BaseRule.apply(this,arguments);
		this.check_on_empty=true;
		this.isValid=function ()
		{
			return (this.value+"").length>0
		}
	},
	// Правило проверки Email
	'email':function ()
	{
		BaseRule.apply(this,arguments);

		this.pattern=/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

		this.isValid=function ()
		{
			return this.pattern.test(this.value.toLowerCase())
		}
	},
	//Правило проверки номера телефона
	'phone':function ()
	{
		BaseRule.apply(this,arguments);

		this.replace_pattern=/[^\d]+/;
		this.pattern=/^[\d]{12}$/;
		this.isValid=function ()
		{
			var val=this.value.replace(this.replace_pattern,'');
			return this.pattern.test(val);
		}
	},
	//Правило проверки минимальной длинны
	'min-length':function ()
	{
		BaseRule.apply(this,arguments);

		this.isValid=function ()
		{
			return this.value.length>=this.params[0];
		}
	},
	//Правило проверки максимальной длинны
	'max-length':function ()
	{
		BaseRule.apply(this,arguments);

		this.isValid=function ()
		{
			return this.value.length<=this.params[0];
		}
	},
	//Правило проверки подтверждения пароля
	'password-confirmation':function ()
	{
		BaseRule.apply(this,arguments);

		this.isValid=function ()
		{
			return this.value==this.data[this.f_name+'_confirmation'].value;
		}
	},
	// Правило проверки файла
	'file':function ()
	{
		this.check_on_empty=true;
		BaseRule.apply(this,arguments);

		this.isValid=function ()
		{
			if(!this.value)
			{
				this.error_type='file_required';
				return typeof this.params[1]=='undefined';
			}
			if(typeof this.params[0]!='undefined'
				&& this.value.size>this.params[0]*1024*1024)
			{
				this.error_type='file_big_size';
				return false;
			}

			return true;
		}
	},
	// Правило проверки изображения
	'image':function ()
	{
		this.pattern=/^image\/(png|gif|jpeg|jpg)$/;
		BaseRule.apply(this,arguments);
		this.isValid=function ()
		{
			if(this.value && !this.pattern.test(this.value.type))
				return false;
			return true;
		}
	},
	// Комбинированное правило проверки Email или номера телефона
	'email-or-phone':function ()
	{
		var args=$.extend([],arguments);
		args.unshift(null);
		BaseRule.apply(this,arguments);
		this.isValid=function ()
		{
			var email=new (Function.prototype.bind.apply(validation_rules.email,args));

			var phone=new (Function.prototype.bind.apply(validation_rules.phone,args));
			return email.isValid() || phone.isValid();
		}
	}

};
// Наследование
for(var key in validation_rules)
{
	var rule=validation_rules[key];
	rule.prototype = Object.create(BaseRule.prototype);
}