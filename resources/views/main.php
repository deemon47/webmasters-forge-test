<?php
$view->title='main';

include_once('_alert.php');
$view->content=function()use($view){?>
<p>
	<?php app('trans','main.introduce_text')?>
	<a href="<?php app('url','main/about')?>"><?php app('trans','main.titles.about')?></a>
</p>
<p>
	<?php app('trans','main.for_start')?>
	<a href="<?php app('url','auth/signup')?>"><?php app('trans','main.signup')?></a>.
</p>
<?php alert('main.for_sigup')?>
<p>
	<?php app('trans','main.if_you_are_ready')?>
</p>

<?php alert('main.for_login')?>

	<?php if($view->last_user){?>
		<div class="panel panel-success">
			<div class="panel-heading"><?php app('trans','main.last_registred_user')?></div>
			<div class="panel-body">

				<div class="row">
					<div class="col-sm-4">
						<div class="thumbnail avatar" >
							<img src="<?php echo Auth::getUserAvatar($view->last_user) ?>" >
						</div>
					</div>
					<div class="col-sm-4">
						<?php echo $view->last_user['full_name']?>
					</div>
					<div class="col-sm-4">
						<?php echo $view->last_user['created_at']?>
					</div>
			</div>
			</div>
		</div>
	<?php }?>
<?php };
include 'layout.php';