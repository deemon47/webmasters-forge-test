<?php
$view->title='404';
$view->content=function(){?>
<div class="text-center">
	<p>
		<?php app('trans','main.page_not_found')?>
	</p>
	<p>
		 <?php app('trans','main.may_be_helpful')?><a href="<?php app('url','')?>"><?php app('trans','main.main_page')?></a>
	</p>

</div>
<?php };
include 'layout.php';