<?php function alert($message,$icon='info-circle',$type='info'){?>
<div class="alert alert-<?php echo $type?>">
		<i class="fa fa-<?php echo $icon; ?>"></i>
		<?php app('trans',$message)?>
	</div>
<?php
}