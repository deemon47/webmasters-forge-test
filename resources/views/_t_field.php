<?php function t_field($field_name,$icon,$type='text',$attr_arr=[]){
	$attr_str='';
	if($attr_arr)
	{
		if(isset($attr_arr['title']))
		{
			$attr_arr['data-toggle']='tooltip';
			$attr_arr['data-placement']='top';
		}
		foreach($attr_arr as $name=>$val)
			$attr_str.=$name.'="'.$val.'" ';
	}
?>
<div class="form-group">
	<label for="<?php echo $field_name?>"><?php $title=app('trans','fields.'.$field_name)?></label>
	<div class="input-group">
		<span class="input-group-addon"><i class="fa fa-<?php echo $icon?>"></i></span>
		<input type="<?php echo $type?>" class="form-control" name="<?php echo $field_name?>" id="<?php echo $field_name?>" placeholder="<?php echo $title?>" <?php echo $attr_str?>>
	</div>
</div><?php
}