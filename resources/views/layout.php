<?php
include_once('_t_field.php');
include_once('_alert.php');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="csrf_token" content="<?php  echo app()->getCSRFToken()?>">
	<title><?php $title=app('trans','main.titles.'.$view->title)?></title>
	<link rel="stylesheet" href="/css/site.min.css">
</head>
<body>
<!-- Навигация  -->
<nav class="navbar navbar-default">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main_nav" >
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="<?php app('url','')?>">
				<?php app('trans','main.site_name');?>
			</a>
		</div>

		<div class="collapse navbar-collapse" id="main_nav">

		   <ul class="nav navbar-nav">
				<li><a href="<?php app('url','main/about')?>"><?php app('trans','main.titles.about')?></a></li>

	 		</ul>

			<!-- Кнопки личного кабинета -->
			<form class="navbar-form navbar-right">
				<?php if(!Auth::check())
				{?>
					<a href="<?php app('url','auth/signup')?>" class="btn btn-success">
						<i class="fa fa-user-plus"></i>
						<?php app('trans','main.titles.signup');?>
					</a>
					<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal_login">
						<i class="fa fa-sign-in"></i>
						<?php app('trans','main.login');?>
					</button>
				<?php }
				else
				{?>
					<a href="<?php app('url','auth/logout')?>" class="btn btn-success">
						<i class="fa fa-sign-out"></i>
						<?php app('trans','main.logout');?>
					</a>
				<?php }?>
			</form>
			<!-- Выбор языка -->
	   	<ul class="nav navbar-nav navbar-right">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" >
						<i class="fa fa-flag"></i>
						<?php app('trans','main.locales.'.app('getLocale'))?>
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<?php foreach(app('config','available_locales') as $locale){

						?>
							<li><a href="<?php app('echoLocaledCurentUrl',$locale)?>">
								<?php app('trans','main.locales.'.$locale)?>

								</a></li>
						<?php } ?>
					</ul>
				</li>
	 			<?php if(Auth::check())
				{?>

					<li><a href="<?php app('url','main/profile')?>"><?php app('trans','main.titles.profile')?></a></li>
				<?php }?>
 			</ul>

		</div>
	</div>
</nav>
<div class="container">
	<h1><?php echo $title?></h1>
	<?php echo $view->content;?>
</div>

<!-- Подвал -->
<footer class="footer">
	<div class="container text-center">
		&copy; <?php app('trans','main.site_name');?>
	</div>
</footer>

<!-- Модальное окно авторизации -->
<form action="<?php app('url','auth/login')?>" >
	<div id="modal_login" class="modal fade" tabindex="-1" >
		<div class="modal-dialog" >
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" ><span>&times;</span></button>
					<h4 class="modal-title"><?php app('trans','main.login');?></h4>
				</div>
				<div class="modal-body">

						<?php alert('auth.for_look_to_profile')?>
						<?php t_field('email_or_phone','user','text',['title'=>app('trans','auth.phone_format',[],true)])?>
						<?php t_field('password','lock','password',$password_attributes=['title'=>app('trans','auth.password_min_length',[],true)])?>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">
						<i class="fa fa-ban"></i>
						<?php app('trans','main.cancel');?>

					</button>
					<button type="submit" class="btn btn-primary">
						<i class="fa fa-sign-in"></i>
						<?php app('trans','main.login');?>
					</button>
				</div>
			</div>
		</div>
	</div>
</form>
<!-- Модальное окно сообщения -->
<div id="modal_message" class="modal fade" tabindex="-2" >
	<div class="modal-dialog" >
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" ><span>&times;</span></button>
				<h4 class="modal-title"><?php app('trans','main.message');?></h4>
			</div>
			<div class="modal-body"></div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success" data-dismiss="modal">ОК</button>
			</div>
		</div>
	</div>
</div>

<script src="<?php app('url','main/locale')?>"></script>
<script src="/js/site.min.js"></script>

<script>
	$(function ()
	{
		app.addForm('<?php app('url','auth/login')?>',<?php Controllers\AuthController::getRulesForJS('postLogin')?>);
		<?php echo $this->js_config?>
	})
</script>
</body>
</html>