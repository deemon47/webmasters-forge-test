<?php
return[
	'id'=>'Идентификатор',
	'full_name'=>'ФИО',
	'email'=>'Email',
	'password'=>'Пароль',
	'password_confirmation'=>'Подтверждение пароля',
	'phone'=>'Номер телефона',
	'avatar'=>'Аватар',
	'email_or_phone'=>'Email или номер телефона',
	'created_at'=>'Дата регистрации',
];