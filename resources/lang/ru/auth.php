<?php
return
[
	'for_start_using'=>'Для начала работы с сайтом Вам нужно зарегистрироваться',
	'password_min_length'=>'Длинна пароля не меньше 6 символов',
	'star_fields'=>'Поля помеченные *(звездочкой) обязательны к заполнению',
	'phone_format'=>'Номер телефона должен быть в международном формате(12 цифр), с кодом города',
	'user_email_alerady_exists'=>'Пользователь с таким <b>Email уже существует</b>',
	'user_phone_alerady_exists'=>'Пользователь с таким <b>номером телефона уже существует</b>',

	'for_look_to_profile'=>'Для того чтобы посмотреть на свой профиль, Вам нужно войти в личный кабинет',
	'user_does_not_exists'=>'Пользователь с такими данными не найден, пожалуйста, проверьте введенные данные и попробуйте снова',
];