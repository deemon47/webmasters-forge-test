<?php
return[
	'id'=>'Identifier',
	'full_name'=>'Full name',
	'email'=>'Email',
	'password'=>'Password',
	'password_confirmation'=>'Password confirmation',
	'phone'=>'Phone number',
	'avatar'=>'Avatar',
	'email_or_phone'=>'Email or phone number',
	'created_at'=>'Registration date',
];