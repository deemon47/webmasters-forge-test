<?php
return
[
	'required'=>'The ":field" <b>required</b>',
	'email'=>'The Email is <b>invalid</b>',
	'phone'=>'The phone number format is <b>invalid</b>',
	'min-length'=>'The length of ":field" must be <b>greater then :0</b>',
	'max-length'=>'The length of  ":field" must be <b>less then :0</b>',
	'password-confirmation'=>'The passwords <b>do not match</b>',
	'file_required'=>'The file ":field" must be uploaded',
	'file_upload_error'=>'Error while file  ":field" uploading, please, try send form again',
	'file_big_size'=>'The file size of ":field" must less then <b>:0 Mb</b>.',
	'email-or-phone'=>'The phone or email is invalid ',
];