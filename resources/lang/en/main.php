<?php
return [
	'site_name'=>'WM Forge',
	'titles'=>
	[
		'main'=>'Main page',
		'about'=>'About site',
		'signup'=>'Sign up',
		'404'=>'404. The page not found',
		'profile'=>'Profile',
	],
	'locales'=>
	[
		'ru'=>'Русский',
		'en'=>'English',
	],
	'login'=>'Login',
	'introduce_text'=>'Hello, my name is Dmitriy. At first, excuse me for my bad English, lately I have no practice in it. And now let me introduce this sitе. Here you can sign up and look at your profile.
	This site was created as test work, about which you can read at page',
	'for_start'=>'At first you must ',
	'signup'=>'sign up',
	'for_sigup'=>'For that click on <b>the green button</b> at top right site corner.',
	'if_you_are_ready'=>'If you already signed up, you can look at your profile after login',
	'for_login'=>'For that you must click on <b>the blue button</b> at top right site corner',
	'token_mismatch'=>'Session expired, the page will be reloaded',
	'message'=>'Message',
	'unknown-error'=>'Unknown error',
	'page_not_found'=>'Unfortunately The page you looking for is not found.',
	'may_be_helpful'=>'May be you can help got to the ',
	'main_page'=>'main page',
	'logout'=>'Logout',
	'cancel'=>'Cancel',
	'last_registred_user'=>'Last signed up user',

];