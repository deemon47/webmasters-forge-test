<?php
return
[
	'for_start_using'=>'For start using the site, please sign up',
	'password_min_length'=>'The password length must greater then 6',
	'star_fields'=>'The field marked with *(star) are required',
	'phone_format'=>'The phone number must be in international format (12 digits) with city code',
	'user_email_alerady_exists'=>'The user with this <b>Email already exists</b>',
	'user_phone_alerady_exists'=>'The user with this <b>phone number already exists</b>',

	'for_look_to_profile'=>'For look at your profile, you must login',
	'user_does_not_exists'=>'The user account not found, please, recheck input and try again',
];