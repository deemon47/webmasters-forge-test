<?php
use Exceptions\ErrorException;
// Регистрация автозагрузчика классов
spl_autoload_register(function ($class)
{
	$path=ROOT_PATH.'app/'.str_replace('\\', '/', $class).'.php';
	if(file_exists($path))
		include_once $path;
});

// Регистрация обработчика исключений
set_exception_handler(function ($e)
{
	$ex_to_string=function ($e)
	{
		$ex_arr=
		[
			'message'=>app('trans','main.unknown-error'),
			'error'=>$e->getMessage(),
			'code'=>$e->getCode(),
			'file'=>$e->getFile().' : '.$e->getLine(),
			'trace'=>$e->getTrace(),
		];
		return json_encode($ex_arr,JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE );
	};

	$err_string=$ex_to_string($e);
	app()->log($err_string,'error');

	http_response_code(500);

	if(method_exists($e, 'render'))
	{
		try
		{
			$e->render();
			return;
		}
		catch (\Throwable $ex)
		{
			$err_string=$ex_to_string($ex);
		}

	}
	header('Content-Type: application/json');
	echo $err_string;

});
// Регистрация обработчика ошибок
set_error_handler(function($err_no, $err_str, $err_file, $err_line, $err_context)
{
	throw new ErrorException($err_no, $err_str, $err_file, $err_line, $err_context);
});

$app=new App();
// alias для удобного вызова
function app($method=null,...$params)
{
	global $app;
	if(!is_null($method))
		return call_user_func_array([$app,$method], $params);
	return $app;
}
app()->start();