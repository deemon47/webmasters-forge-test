<?php
namespace Exceptions;


/**
 *  Исключение в случае не верно заполненных данных формы
 */
class ValidationErrorException  extends BaseException
{
	protected $validator;
	/**
	 * @param Validation\Validator $validator Объект проверки данных
	 */
	public function __construct($validator)
	{
		$this->validator=$validator;
	}
	public function render()
	{
		http_response_code(422);
		echo json_encode(['validation_errors'=>$this->validator->getErrors()]);
	}
}