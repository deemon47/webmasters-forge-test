<?php
namespace Exceptions;


/**
 *  php core error exception
 */
class ErrorException  extends BaseException
{
	public function __construct($code, $message, $file, $line, $conext)
	{
		$this->message=$message ;
		$this->code=$code ;
		$this->file=$file ;
		$this->line=$line ;
	}
}