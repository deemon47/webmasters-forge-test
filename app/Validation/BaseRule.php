<?php

namespace Validation;

/**
 * Базовый функционал правила проверки
 */
abstract class BaseRule
{
	protected $field_name=null;

	protected $params=null;
	protected $error_type=null;
	protected $check_on_empty=false;
	/**
	 * @param string $field_name  имя поля
	 * @param array $params     дополнительные параметры правила
	 */
	public function __construct($field_name,$params)
	{
		$this->field_name=$field_name;
		$this->params=$params;
	}
	/**
	 * Дополнительный метод необходимости проверки, чтобы исключить  проверку на то заполнено ли поле в каждом правиле
	 * @param  Validation\Data $data Объект данных запроса
	 * @return boolean       нужно ли вызывать метод isValid
	 */
	public function needCheck($data)
	{
		return $this->check_on_empty
			|| !empty($data->{$this->field_name});
	}
	/**
	 * Основная логика проверки правила
	 * @param  Validation\Data  $data Объект данных запроса
	 * @return boolean       Данные верны
	 */
	abstract public function isValid($data);

	/**
	 * Генерация имени ошибки для вывода локализованного сообщения
	 * @return string название правила через тире
	 */
	public function getErrorType()
	{
		if($this->error_type)
			return $this->error_type;

		$type=basename(static::class);
		$type=str_replace('Rule', '', $type);
		$type=preg_replace('/([A-Z])/', '-$1', $type);
		$type=trim($type,'-');
		$type=strtolower($type);

		return $type;
	}
	/**
	 * Генерация сообщения об ошибке
	 * @return string локализованное сообщение об ошибке
	 */
	public function getError()
	{
		$data=$this->params;
		$data['field']=app('trans','fields.'.$this->field_name);
		return app('trans','validation.'.$this->getErrorType(),$data);
	}
}