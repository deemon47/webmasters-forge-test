<?php

namespace Validation;

/**
 * Правило проверки телефона или email
 */
class EmailOrPhoneRule extends EmailRule
{
	public function isValid($data)
	{
		if(!parent::isValid($data))
		{
			$phone=new PhoneRule($this->field_name,$this->params);
			return $phone->isValid($data);
		}
		return true;
	}
}