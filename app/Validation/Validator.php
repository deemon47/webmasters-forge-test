<?php

namespace Validation;
use Exceptions\BaseException;
use Helpers;

/**
 * Проверка данных
 */
class Validator
{
	private $data=null;

	private $rules=null;

	private $filters=[];

	private $post_filters=[];

	private $valid_data=null;

	private $is_valid=null;

	private $errors=[];
	/**
	 * @param array $rules правила проверки
	 * @param array $data  данные запроса
	 */
	public function __construct($rules,$data)
	{
		if(isset($rules['_filters']))
		{
			$this->filters=$rules['_filters'];
			unset($rules['_filters']);
		}
		if(isset($rules['_post_filters']))
		{
			$this->post_filters=$rules['_post_filters'];
			unset($rules['_post_filters']);
		}

		$this->rules=$rules;

		$this->data= new Data($data,array_keys($rules));
	}
	/**
	 * Применение функций обратного вызова к данным
	 * @param  Validation\Data &$data     Объект данных
	 * @param  array $callbacks массив функций обратного вызова
	 * @return void
	 */
	protected function applyCallbacks(&$data,$callbacks)
	{
		foreach($callbacks as $f_name=>$callback)
			if(isset($data->$f_name))
				$data->$f_name=call_user_func_array($callback, [$data->$f_name]);

	}
	/**
	 * Применение фильтров
	 * @return void
	 */
	public function applyFilters()
	{
		$this->applyCallbacks($this->data,$this->filters);
	}
	/**
	 * Примение после проверочных фильтров
	 * @return void
	 */
	public function applyPostFilters()
	{
		$this->applyCallbacks($this->valid_data,$this->post_filters);
	}
	/**
	 * Проверка верности входных данных
	 * @return boolean данные верны
	 */
	public function isValid()
	{
		if(!is_null($this->is_valid))
			return $this->is_valid;

		$is_valid=true;
		$this->applyFilters();
		foreach($this->rules as $field_name=>$rules)
		{
			foreach($rules as $ind=>$rule)
			{
				$rule=explode(':', $rule);
				$rule_type=array_shift($rule);
				$params=$rule;

				$rule_class='\\Validation\\'.Helpers::dashToCamelCase($rule_type).'Rule';

				if(!class_exists($rule_class))
					throw new BaseException("Validation rule doesn't exists: ".$rule_class);

				$rule=new $rule_class($field_name,$params);

				if($rule->needCheck($this->data)
					&& !$rule->isValid($this->data))
				{
					$this->errors[$field_name]=$rule->getError();
					$is_valid=false;
					break;
				}
			}
		}
		$this->valid_data=$this->data;

		$this->applyPostFilters();

		return  $this->is_valid=$is_valid;
	}
	/**
	 * Возвращает объект проверенных данных
	 * @return Validation\Data правильные данные
	 */
	public function getValidData()
	{
		return $this->valid_data;
	}
	/**
	 * Возвращает массив ошибок выявленных при проверке входных данных
	 * @return array массив ошибок
	 */
	public function getErrors()
	{
		return $this->errors;
	}
}