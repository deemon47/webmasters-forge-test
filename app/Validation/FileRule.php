<?php

namespace Validation;

/**
 * Правило проверки файла
 */
class FileRule extends BaseRule
{
	protected $check_on_empty=true;

	public function isValid($data)
	{
		// Второй параметр указывает на обязательность загрузки файла
		if(empty($_FILES[$this->field_name]))
		{
			$this->error_type='file_required';
			return empty($this->params[1]);
		}

		$file_info=$_FILES[$this->field_name];
		if($file_info['error'])
		{
			$this->error_type='file_upload_error';
			return false;
		}

		// Первый параметр указывает на максимальный размер файла в мегабайтах
		if(!empty($this->params[1]) && $file_info['size']>$this->params[1]*1024*1024)
		{
			$this->error_type='file_big_size';
			return false;
		}

		return true;
	}
}