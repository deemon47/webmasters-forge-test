<?php

namespace Validation;

/**
 * Правило проверки является ли файл изображением
 */
class ImageRule extends BaseRule
{
	protected $check_on_empty=true;

	protected $pattern='/^image\/(png|gif|jpeg|jpg)$/';

	public function isValid($data)
	{
		if(isset($_FILES[$this->field_name])
			&& !preg_match($this->pattern, $_FILES[$this->field_name]['type']))
			return false;
		return true;
	}
}