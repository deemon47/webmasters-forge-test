<?php

namespace Validation;

/**
 * Правило подтверждения пароля
 */
class PasswordConfirmationRule extends BaseRule
{
	public function isValid($data)
	{
		$confirmation_field=$this->field_name.'_confirmation';
		return $data->{$this->field_name}==$data->$confirmation_field;
	}
}