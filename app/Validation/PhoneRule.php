<?php

namespace Validation;

/**
 * Правило номер телефона
 */
class PhoneRule extends BaseRule
{
	protected $pattern='/^[\d]{12}$/';
	protected $replace_pattern='/[^\d]+/';
	public function isValid($data)
	{
		$value=preg_replace($this->replace_pattern,'',$data->{$this->field_name});
		$data->{$this->field_name}=$value;
		return preg_match($this->pattern, $value);
	}
}