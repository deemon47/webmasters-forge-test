<?php
namespace Validation;

/**
 * Проверка минимальной длинны
 */
class MinLengthRule extends BaseRule
{

	public function isValid($data)
	{
		return mb_strlen($data->{$this->field_name}) >= $this->params[0];
	}
}