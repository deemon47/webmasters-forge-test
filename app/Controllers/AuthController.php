<?php

namespace Controllers;
use View;
use Auth;
use DB;
/**
 * Авторизация и регистрация
 */
class AuthController extends BaseController
{
	/**
	 * Правила проверки формы авторизации
	 * @return array правила
	 */
	public static function postLoginRules()
	{
		return [
			'email_or_phone'=>['required','email-or-phone'],
			'password'=>['required','min-length:6'],
			'_filters'=>
			[
				'email_or_phone'=>'strtolower',
			]
		];
	}
	/**
	 * Обработка формы авторизации
	 * @param  Validation\Data $valid_data Проверенные данные
	 * @return mixed             ответ
	 */
	public function postLogin($valid_data)
	{
		$field=strpos($valid_data->email_or_phone, '@')===false
			?'phone'
			:'email';

		$result=DB::query("SELECT * FROM users WHERE $field=:$field limit 1",[
			$field=>$valid_data->email_or_phone
		]);
		$user=$result->fetch();
		if($user
			&& Auth::verifyPassword($valid_data->password,$user['password']))
		{
			Auth::setLogin($user);
			return [
				'redirect'=>app('url','main/profile'),
			];
		}
		$this->error('auth.user_does_not_exists');
	}
	/**
	 * Вывод формы регистрации
	 * @return view ответ
	 */
	public function getSignup()
	{
		if(Auth::check())
			app('redirect',app('url','main/profile'));

		return new View('signup');
	}
	/**
	 * Правила проверки формы регистрации
	 * @return array правила
	 */
	public static function postSignupRules()
	{
		return
		[
			'full_name'=>['max-length:255'],
			'email'=>['required','email','max-length:255'],
			'phone'=>['phone'],
			'password'=>['required','min-length:6','password-confirmation'],
			'password_confirmation'=>['required'],
			'avatar'=>['file:3','image'],
			'_filters'=>
			[
				'phone'=>function ($value)
				{
					return preg_replace('/[^\d]+/', '', $value);
				},
				'email'=>'strtolower',
			],
			'_post_filters'=>
			[
				'password'=>[Auth::class,'encriptPassword'],
			]
		];
	}
	/**
	 * обработка формы регистрации
	 * @param  Validation $valid_data Проверенные данные
	 * @return response             ответ
	 */
	public function postSignup($valid_data)
	{

		$check_phone='';
		unset($valid_data->password_confirmation);
		unset($valid_data->avatar);
		$query_data=$valid_data->only(['phone','email']);
		if($valid_data->phone)
			$check_phone='OR phone LIKE :phone';
		else
			unset($query_data['phone']);

		$user=DB::query("SELECT * FROM users WHERE email LIKE :email $check_phone LIMIT 1",$query_data)
			->fetch();
		if($user)
		{
			if($user['email']==$valid_data->email)
				$this->error('auth.user_email_alerady_exists');
			else
				$this->error('auth.user_phone_alerady_exists');
		}

		$user_data=$valid_data->toArray(true);
		if(isset($_FILES['avatar']))
		{
			$file=$_FILES['avatar'];

			$file_ext=preg_replace('/^.+\.(\w+)$/', '$1', $file['name']);
			$path=sprintf('images/avatar-%s.%s',microtime(),$file_ext);

			$user_data['avatar']='/'.$path;

			$path=PUBLIC_PATH.$path;

			if(!move_uploaded_file($file['tmp_name'],$path))
				$this->error('validation.file_upload_error');
		}

		$user_id=DB::insert('users',$user_data);
		$user_data['id']=$user_id;
		Auth::setLogin($user_data);
		return [
			'redirect'=>app('url','main/profile'),
		];
	}

	/**
	 * Обработка выхода
	 * @return response ответ
	 */
	public function anyLogout()
	{
		Auth::logout();
		$url=app('url','');

		if(app('isApiRequest'))
		{
			return
			[
				'redirect'=>$url,
			];

		}
		app('redirect',$url);
	}
}