<?php

namespace Controllers;

use Exceptions\MessageException;
use Helpers;
/**
 * Базовый контроллер для реализации общих методов и стандартизации
 */
abstract class BaseController
{
	/**
	 * Генерация правил проверки для передачи на пользовательскую часть
	 * @param  string $method_name имя метода контроллера, правила для которого нужны
	 * @return array              JSON строка правил
	 */
	public static function getRulesForJS($method_name)
	{
		$method_name.='Rules';
		$rules_str= json_encode(Helpers::array_except(static::$method_name(),['_filters','_post_filters']));
		echo $rules_str;
		return $rules_str;
	}

	/**
	 * Инициализация вывода сообщения пользователю в виде всплывающего окна
	 * @param  string $message сообщение
	 * @return void
	 */
	public function error($message)
	{
		throw new MessageException(app('trans',$message), 'error');
	}
}