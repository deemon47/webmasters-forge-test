<?php

use Exceptions\BaseException;
use Exceptions\NotFoundException;
use Exceptions\ValidationErrorException;
use Exceptions\TokenMismatchException;
use Validation\Validator;

/**
 * Ядро приложения
 */
class App
{

	private $default_locale=null;

	private $current_locale=null;

	private $is_api_request=false;
	protected $request_data=null;

	public $echo_trans=false;
	protected $link_without_locale='';
	/**
	 * Генерация текущей ссылки с возможностью выбрать локализацию
	 * @param  string $locale локализация
	 * @return string         URL
	 */
	public function echoLocaledCurentUrl($locale)
	{
		return $this->url($this->link_without_locale,$this->getLocalePrefix($locale));
	}
	/**
	 * Возвращает текущую локализацию
	 * @return string локализация
	 */
	public function getLocale()
	{
		return $this->current_locale;
	}
	/**
	 * Возвращает  true в случае если API запрос
	 * @return boolean
	 */
	public function isApiRequest()
	{
		return $this->is_api_request;
	}
	/**
	 * Возвращает префикс локализации, исключая сегмент локализации по умолчанию
	 * @param  string $locale локализация
	 * @return string         URL префикс
	 */
	public function getLocalePrefix($locale=null)
	{
		if(is_null($locale))
			$locale=$this->current_locale;

		return '/'.($locale==$this->default_locale
			?''
			:$locale.'/');
	}
	/**
	 * Метод получения настроек
	 * @param  string $config_path условное имя настройки
	 * @param  mixed $default     занчение по умолчанию
	 * @return mixed              занчение настройки
	 */
	public function config($config_path,$default=null)
	{
		return Helpers::getArrayFileValue(Helpers::getPath(''),'config.'.$config_path,$default);
	}
	/**
	 * Безопасный запуск php сессии
	 * @return void
	 */
	public function startSession()
	{
		if(session_status() == PHP_SESSION_NONE)
			session_start();
	}
	/**
	 * Генерация случайной строки
	 * @return string случайная строка
	 */
	public function getRand()
	{
		return  bin2hex(random_bytes(32));
	}
	/**
	 * Возвращает и генерирует CSRF токен если необходимо
	 * @return string CSRF токен
	 */
	public function getCSRFToken()
	{
		$this->startSession();
		if(empty($_SESSION['csrf_token']))
			$_SESSION['csrf_token']=$this->getRand();

		setcookie('csrf_token', $_SESSION['csrf_token'],0 ,'/');
		return $_SESSION['csrf_token'];
	}
	/**
	 * Проверка наличия и соответствия CSRF токена
	 * В случае ошибки выбрасывается исключение
	 * @return void
	 */
	public function checkCSRFToken()
	{
		if(empty($_COOKIE['csrf_token']))
			throw new TokenMismatchException("HasNoToken");

		if ($_COOKIE['csrf_token'] !== $this->getCSRFToken())
			throw new TokenMismatchException("TokenMismatch");
	}
	/**
	 * Генерирует и возвращает локализованную строку
	 * @param  string  $translation_path условный путь к шаблону локализации
	 * @param  array  $params           массив параметров замены по шаблону
	 * @param  boolean $dont_echo        если true то не выводит в поток
	 * @return string                    локализованный текст
	 */
	public function trans($translation_path,$params=null,$dont_echo=false)
	{

		// Поиск по указанной локали
		$trans=Helpers::getArrayFileValue(Helpers::getPath('resources/lang/'.$this->current_locale.'/'),$translation_path);

		if(!is_null($trans) && $params)
		{
			$fields=array_keys($params);
			$fields=array_map(function ($el)
			{
				return ':'.$el;
			}, $fields);

			$trans=str_replace($fields, $params, $trans);
		}
		// Чтобы удобнее было добавлять недостающий текст вывод путя перевода
		if(is_null($trans))
			$trans=$translation_path;

		if(!$dont_echo && $this->echo_trans)
			echo $trans;
		return $trans;
	}
	/**
	 * Сохранение данных в датированный лог файл
	 * @param  mixed $data    данные для сохранения
	 * @param  string $comment комментарий к данным
	 * @return this
	 */
	public function log($data,$comment=null)
	{
		if(!is_string($data))
			$data=var_export($data,true);

		$date=new DateTime();
		if(!is_null($comment))
			$comment.=' ';
		$date=$date->format('[Y-m-d H:i:s] ');
		file_put_contents(Helpers::getPath('storage/log.txt'), $date.$comment.$data."\n",FILE_APPEND);

		return $this;
	}
	/**
	 * Перенаправление на другую страницу и прекращение выполнения скрипта
	 * @param  string $url  URL перенаправления, по умолчанию перенаправляет на страницу откуда поступил запрос
	 * @param  string $code код перенаправления
	 * @return void
	 */
	public function redirect($url='back',$code='403')
	{
		if($url=='back')
		{
			$url=$_SERVER['HTTP_REFERER'];
			if(!$url)
				$url=$this->getLocalePrefix();
		}
		http_response_code($code);
		header('Location: '.$url);

		die();
	}
	/**
	 * Запуск приложения
	 * @return void
	 */
	public function start()
	{
		// Разбор URL для маршрутизации по контроллерам
		$uri=$_SERVER['REQUEST_URI'];
		$uri=trim($uri,'/');
		// Формат УРЛ /controller[/method] если не указан метод то вызывается метод Index;
		$controller_name=null;
		$method_name=null;

		// Определение локализации
		$available_locales=static::config('available_locales');
		$locale=$available_locales[0];
		$this->default_locale=$locale;

		$uri_path_arr=explode('/', $uri);
		if(in_array($uri_path_arr[0], $available_locales))
		{
			$locale=array_shift($uri_path_arr);

			if($locale==$this->default_locale)
				$this->redirect('/');
		}
		$depth=count($uri_path_arr);

		$this->current_locale=$locale;

		// Сохранение ссылки без префикса локализации для генерации ссылки для смены локализации
		$this->link_without_locale=implode('/', $uri_path_arr);

		// Получение данных запроса
		$this->loadRequestData();

		// Запуск проверки CSRF токена в случае отправки данных
		if($_SERVER['REQUEST_METHOD']=='POST')
			$this->checkCSRFToken();

		// Определение имени контроллера и метода для обработки запроса
		if($depth==0 || $uri_path_arr[0]=='')
			$controller_name='Main';
		else
			$controller_name=Helpers::dashToCamelCase($uri_path_arr[0]);

		if($depth>1)
			$method_name=Helpers::dashToCamelCase($uri_path_arr[1]);
		else
			$method_name='Index';

		$controller_name='\\Controllers\\'.$controller_name.'Controller';

		// Проверка существования класса и метода
		if(!class_exists($controller_name))
			throw new NotFoundException("ControllerNotFound :".$controller_name);
		$controller=new $controller_name();

		$tmp_method_name=$method_name;
		$method_name1=strtolower($_SERVER['REQUEST_METHOD']).$method_name;
		if(!method_exists($controller, $method_name=$method_name1)
		 		&& !method_exists($controller, $method_name='any'.$tmp_method_name)
		 	)
			throw new NotFoundException("MethodNotFound :".$controller_name.'@'.$method_name.'|'.$method_name1);

		$valid_data=$this->validate($controller,$method_name);

		// Запуск буфера ввода, для защиты от вывода до установки заголовков
		ob_start();
		$error=false;
		try
		{
			$result=call_user_func_array([$controller,$method_name],[$valid_data]);
		}
		catch (\Throwable $e)
		{
			$error=$e;
		}
		$content=ob_get_clean();
		if($error)
			throw $error;

		// Проверка данных ответа и форматирование вывода
		if($result instanceof View)
			$result->render();
		elseif(is_array($result))
		{
			header('Content-Type: application/json');
			echo json_encode($result);
		}
		elseif($result)
			echo $result;
		elseif($content)
			echo $content;

		return $this;
	}
	/**
	 * Загрузка данных запроса
	 * @return array данные запроса
	 */
	public function loadRequestData()
	{
		$content=file_get_contents('php://input');
		$data=$_POST;
		if($content)
		{
			$json=json_decode($content,JSON_OBJECT_AS_ARRAY);
			if($json)
			{
				$data+=$json;
				$this->is_api_request=true;
			}
		}
		if(!empty($data['is_api_request']))
			$this->is_api_request=true;

		$this->request_data=$data;

		return $data;
	}
	/**
	 * Проверка данных запроса
	 * В случае не верно заполненных данных выбрасывается исключение
	 * @param  Controller\BaseController $controller  Контроллер обработки
	 * @param  string $method_name имя метода обработки
	 * @return Validation\Data              Проверенные данные
	 */
	public function validate($controller,$method_name)
	{
		$rules_method_name=$method_name.'Rules';

		if(!method_exists($controller,$rules_method_name))
			return true;

		$rules=call_user_func_array([$controller,$rules_method_name],[]);

		$validator=new Validator($rules,$this->request_data);

		if(!$validator->isValid())
			throw new ValidationErrorException($validator);

		return $validator->getValidData();
	}
	/**
	 * Генерация локализованной URL
	 * @param  string $url           сегмент URL
	 * @param  string $loclae_prefix явное указание префикса
	 * @return string                локализованный URL
	 */
	public function url($url,$loclae_prefix=null)
	{
		if(is_null($loclae_prefix))
			$loclae_prefix=$this->getLocalePrefix();

		$url='http'.(!empty($_SERVER['HTTPS'])?'s':'').'://'
			.$_SERVER['HTTP_HOST']
			.$loclae_prefix
			.$url;

		if($this->echo_trans)
			echo $url;

		return $url;
	}
}
