<?php

use Exceptions\BaseException;
use Validation\Data;
/**
 * Менеджер представления
 */
class View extends Data
{
	protected $file_path;
	/**
	 * @param string $file_name  имя файла шаблона
	 * @param array  $data_array дополнительные параметры для шаблона
	 */
	public function __construct($file_name,$data_array=[])
	{
		$this->file_path=$path=Helpers::getPath('resources/views/'.$file_name.'.php');

		parent::__construct($data_array);

		if(!file_exists($this->file_path))
			throw new BaseException("ViewFileNotFound :".$this->file_path);

	}
	/**
	 * Генерация вывода по шаблону
	 * @return void
	 */
	public function render()
	{
		$view=$this;
		ob_start();
		$error=false;
		try {
			app()->echo_trans=true;
			include_once $this->file_path;

		} catch (\Throwable $e) {
			$error=$e;
		}
		app()->echo_trans=false;
		$content=ob_get_clean();
		if($error)
			throw $error;
		echo $content;
	}
	public function __get($f_name)
	{
		$value=parent::__get($f_name);
		if(is_callable($value))
			$value=call_user_func($value);
		return $value;
	}

}