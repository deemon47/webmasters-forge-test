<?php

use Exceptions\BaseException;
/**
 * Авторизация пользователя
 */
class Auth
{
	/**
	 * Шифрование пароля
	 * @param  string $password пароль
	 * @return string           шифрованный хэш пароля
	 */
	public static function encriptPassword($password)
	{
		return password_hash($password,PASSWORD_BCRYPT);
	}

	/**
	 * Проверка соответствия хэша паролю
	 * @param  string $password пароль
	 * @param  string $hash     хэш пароля
	 * @return boolean           пароль соответствует
	 */
	public static function verifyPassword($password,$hash)
	{
		return password_verify($password,$hash);
	}

	/**
	 * Проверка авторизации пользователя
	 * @return boolean пользователь авторизован
	 */
	public static function check()
	{
		app('startSession');

		if(!empty($_SESSION['user']))
			return true;

		if(!empty($_COOKIE['auth_token']) && !empty($_COOKIE['auth_token_id']))
		{
			$result=DB::query('SELECT * FROM users WHERE token = :token and id=:id limit 1',[
				'token'=>$_COOKIE['auth_token'],
				'id'=>$_COOKIE['auth_token_id'],
			]);
			$user=$result->fetch();
			if($user  && $user['token_expired_at'] >time())
			{

				static::setLogin($user);
			}

		}
		return false;
	}
	/**
	 * Возвращает данные пользователя
	 * @return mixed        массив данных или null
	 */
	public static function user()
	{
		app('startSession');

		if(!empty($_SESSION['user']))
			return $_SESSION['user'];

		return null;
	}
	/**
	 * Авторизовать данного пользователя и сгенерировать временный токен
	 * @param array $user данные пользователя
	 */
	public static function setLogin($user)
	{
		$id=$user['id'];

		$token=app('getRand');
		$token_expired_time=time()+app('config','token_lifetime')*60*60*1000;

		$token_expired_at=DB::time($token_expired_time);

		$data=compact('id','token','token_expired_at');
		$is_ok=DB::update('users',$data,'id=:id');
		if($is_ok)
		{
			$user['token']=$token;
			$user['token_expired_at']=$token_expired_time;
			app('startSession');
			$_SESSION['user']=$user;
			setcookie('auth_token', $token, $token_expired_time ,'/');
			setcookie('auth_token_id', $id, $token_expired_time ,'/');
			return true;
		}
		return false;
	}
	/**
	 * Возвращает URL на аватар пользователя или на изображение "нет аватара"
	 * @param  array $user пользователь, если не указано авторизованный
	 * @return string       URL на изображение аватара
	 */
	public static function getUserAvatar($user=null)
	{
		if(is_null($user))
			$user=static::user();
		if($user && !empty($user['avatar']))
			return $user['avatar'];
		return '/images/no_avatar.png';
	}
	/**
	 * Прекратить сессию пользователя
	 * @return void
	 */
	public static function logout()
	{

		app('startSession');
		unset($_SESSION['user']);
		setcookie('auth_token', '', 0 ,'/');
		setcookie('auth_token_id', '', 0 ,'/');
	}
}