<?php
use Exceptions\BaseException;
/**
 * Класс работы с базой данных
 */
class DB
{
	/**
	 * Соединение с базой данных использую настройки из конфигурации
	 * @return PDO объект работы с базой данных
	 */
	public static function connect()
	{
		$dsn='mysql:dbname='.app('config','database.db_name')
		.';host='.app('config','database.host')
		.';port='.app('config','database.port')
		.';charset='.app('config','database.charset');

		return new PDO($dsn,app('config','database.user_name'),app('config','database.password'));
	}
	/**
	 * Безопасное выполнение запроса с защитой параметров
	 * @param  string $query       запрос
	 * @param  array  $params      параметры для модификации подстановки в текст запроса
	 * @param  out DBHandler &$db_handler объект подключения к базе данных
	 * @return PDOStatement               результат выполнения запроса
	 */
	public static function query($query,$params=[],&$db_handler=null)
	{
		$db_handler=static::connect();
		$query_statement=$db_handler->prepare($query);
		$new_keys=array_map(function ($el)
		{
			return ':'.$el;
		}, array_keys($params));
		$result=$query_statement->execute(array_combine($new_keys,$params));
		if(!$result)
			throw new BaseException("MYSQL ERROR: ".implode(' , ', $query_statement->errorInfo()));

		return $query_statement;
	}
	/**
	 * Преобразования времени в формат базы данных
	 * @param  int $time время в миллисекундах
	 * @return string       дата
	 */
	public static function time($time=null)
	{
		if(is_null($time))
			$time=time();
		return date("Y-m-d H:i:s", $time);
	}
	/**
	 * Формирование SET части запроса
	 * @param  array $data данные для формирования
	 * @return string       SET часть запроса
	 */
	public static function preapreSetPart($data)
	{
		$query=[];
		foreach($data as $f_name=>$val)
			$query[]="$f_name=:$f_name ";
		return " SET ".implode(',', $query);
	}
	/**
	 * Выполнение запроса обновления
	 * @param  string $table_name имя таблицы
	 * @param  array &$data      данные для обновления
	 * @param  string $condition  условие запроса
	 * @return int             количество задействованных строк
	 */
	public static function update($table_name,&$data,$condition=null)
	{
		if(!is_null($condition))
			$condition=' WHERE '.$condition;
		$data['updated_at']=static::time();
		$query="UPDATE $table_name ".static::preapreSetPart($data).$condition;
		$result=static::query($query,$data,$db_handler);
		return $result->rowCount();
	}
	/**
	 * Выполнение запроса добавления
	 * @param  string $table_name имя таблицы
	 * @param  array &$data      данные для добавления
	 * @return int             ID добавленной записи
	 */
	public static function insert($table_name,&$data)
	{
		$data['created_at']=static::time();
		$query="INSERT INTO $table_name ".static::preapreSetPart($data);
		$result=static::query($query,$data,$db_handler);
		return $db_handler->lastInsertId();
	}

}